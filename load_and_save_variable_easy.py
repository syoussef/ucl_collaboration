import pickle
import numpy as np

def save(filename, *args): # global version
    # Get global dictionary
    glob = globals()
    d = {}
    for v in args:
        # Copy over desired values
        d[v] = glob[v]
    with open(filename, 'wb') as f:
        # Put them in the file 
        pickle.dump(d, f)

def load(filename): # gobal version
    # Get global dictionary
    glob = globals()
    with open(filename, 'rb') as f:
        for k, v in pickle.load(f).items():
            # Set each global variable to the value from the file
            glob[k] = v

def save_specific_value(filename, *args): # Nouvelle version sans variables globales => marche dans les fonctions
    tab = []
    for v in args:
        tab.append(v)

    with open(filename, 'wb') as f:
        # Put them in the file 
        pickle.dump(tab, f)

def load_return(filename): 
    tab = []
    with open(filename, 'rb') as f:
        # print("test:")
        # print(pickle.load(f))
        for v in pickle.load(f):
            # print(v)
            # Set each global variable to the value from the file
            # glob[k] = v
            tab.append(v)
    return tab

def test():
    a = [np.array([3,4,5]),np.array([5,6,7])]
    b = 4
    print(type(a))
    save_specific_value('nom_fichier',a,b)
    a = 0
    b= 0
    [a,b] = load_return("nom_fichier")
    print(a)
    print(b)

# test()

# a = [1,2,3] # for test
# b = [4,5,6]
# save('nom_fichier','a','b')

# a = 0
# b = 0
# print(a)
# print(b)

# load('nom_fichier')

# print(a)
# print(b)