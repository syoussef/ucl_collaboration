#!/usr/bin/env python;
# -*- coding: utf-8 -*-
import Sofa.Core
import Sofa.constants.Key as Key
from spicy import *
from math import ceil, cos, sin, pi
import numpy as np
import six
import utils as utils

try :
    import Connexion_Function_ucl as connect
except :
    import ucl_collaboration.Connexion_Function_ucl as connect

# Réunit toutes les fonctions python, permettant de réaliser des trajectoires

 ### - FUNCTIONS - ###
 # def smooth_trajectory_start

 ### - PATTERN FUNCTIONS - ###
# Usually return a tab, that contains position that will be 

def SpiralPetoux():
    """Generate the trajectory we want to apply in the scene.

    Note:
    ----
        self.goals: list of 3D goals

    """
    # self.goals = SpiralPatternGenerator(x_zone_size=40,y_zone_size=40,plan_height = 110,point_number=6000)
    goals = []

    # 3D spiral trajectory
    center= np.array([110 - 0.25, 0, 0])
    radius = 25
    n_samples = 200
    height_increment = 0.5 / n_samples
    additional_height = 0.0
    angle_increment = 2 * 6.28319 / n_samples
    additional_angle = 0.0
    
    # Starting goal pos to first goal pos on the trajectory
    steps_to_spiral = 10
    start_spiral_point = np.array([center[0] + additional_height,
                                   center[1] + radius * np.cos(additional_angle),
                                   center[2] + radius * np.sin(additional_angle)])
    for i in range(steps_to_spiral):
        x = center[0] + i * (start_spiral_point[0] - center[0]) / steps_to_spiral
        y = center[1] + i * (start_spiral_point[1] - center[1]) / steps_to_spiral
        z = center[2] + i * (start_spiral_point[2] - center[2]) / steps_to_spiral
        goals.append(np.array([x,y,z]))
    
    for i in range(n_samples - steps_to_spiral):
        x = center[0] + additional_height
        y = center[1] + radius * np.cos(additional_angle)
        z = center[2] + radius * np.sin(additional_angle)
        goals.append(np.array([x,y,z]))

        additional_height += height_increment
        additional_angle += angle_increment

    return goals

def PatternGenerator(x_zone_size,y_zone_size,plan_height,step):
    """
    x_zone_size et y_zone_size : taille de la zone à scanner en mm, en x et en y
    step : pas en mm entre deux points
    """
    pattern_tab = []
    x = -x_zone_size/2 - step
    y = -y_zone_size/2 - step
    nb_x = x_zone_size/step + 1
    nb_y = y_zone_size/step + 1
    for ind_x in range(int(nb_x)):
        x = x + step
        if ind_x%2 == 0 :
            for ind_y in range(int(nb_y)):
                
                pattern_tab.append([plan_height,y,x])
                y = y + step
        else :
            for ind_y in range(int(nb_y)):
                
                pattern_tab.append([plan_height,y,x])
                y = y - step

    return pattern_tab

def SpiralPatternGenerator(x_zone_size,y_zone_size,plan_height,point_number):
    pi = 3.1415
    pattern_tab = []
    
    r_max = max(x_zone_size,y_zone_size)
    sprial_number = ceil(r_max/0.2); # 2 mm per round
    for index_r in np.linspace(0, sprial_number*2*pi, point_number):
        r = index_r
        x=r*cos(index_r);
        y=r*sin(index_r);
        pattern_tab.append([plan_height,y,x])

    return pattern_tab


def CirclePatternGenerator(r , pos_init,nb_iter,h_circle):
    pi = 3.1415
    pattern_tab = []
    nb_iter_d = 50
    h_effector = pos_init[0]
    height_shift = h_effector - h_circle
    
    d = pos_init
    for iter in range(nb_iter_d):
        if d_flag == 0 :
            d[0][1] = (iter/nb_iter_d)*r
            d[0][0] = h_effector - (iter/nb_iter_d)*height_shift
            pattern_tab.append(d)
    for iter in range(nb_iter):
            d[0][1] = r*cos((iter/nb_iter)*2*pi)
            d[0][2] = r*sin((iter/nb_iter)*2*pi)
            pattern_tab.append(d)


    return pattern_tab

 ### - CONTROLLER - ###

class CircleTrajectory(Sofa.Core.Controller):
        """ 
        Génère un trajectoire circulaire à l'effecteur (plan x - y) [ Faire une version OK pour tous les plans ?]

        INPUT : 
        rayon = rayon du cercle de la trajectoire
        nb_iter : nombre d'itérations, de pas de calculs, à effectuer pour effectuer un cercle complet (donne la vitesse à laquelle est exécutée le cercle)
        node : noeud sur se trouve l'objet qui va effectuer un cercle
        name : nom de l'objet qui contient la position à faire varier
        circle_height : hauteur du cercle tracé par l'effecteur (selon l'axe z ici)
        module = variable stiff qui contient toutes les données du robot
        axis = variable qui donne le plan dans lequel on tracer le cercle ([1,1,0] = x,y,  [1,0,1] = x,z)

        Exemple : rootNode.addObject(CircleTrajectory(rayon =circle_radius, nb_iter = nb_iter_circle, node = goal2,name = 'goal2M0',circle_height = circle_height+0,module=stiff,axis = [0,1,1]))

        """
        def __init__(self,rayon,nb_iter,node,name,circle_height,module,axis = [1,1,0],*args, **kwargs):
            Sofa.Core.Controller.__init__(self,args,kwargs)
            self.stiffNode = node # for the generic one
            self.position = self.stiffNode.getObject(name)
            self.nb_iter_d = 50 # nombre d'iteration pour réaliser le sement qui amène au bord u cercle
            self.nb_iter = nb_iter  # nb d'itération pour réaliser un cercle complet
            self.d_flag = 0 # flag, passe à 1 quand le placement sur le cercle est effectué
            self.iter = 1 # numérote (compte) les itérations
            self.r = rayon
            self.circle_height = circle_height
            self.h_effector = module.h_module * module.nb_module
            self.height_shift = self.h_effector - self.circle_height
            # print(self.height_shift)

            flag_temp = 0
            for k in range(len(axis)):
                if axis[k] == 0 :
                    self.perpen = k
                elif flag_temp == 0 :
                    self.p1 = k
                    flag_temp = 1
                else :
                    self.p2 = k

        def onAnimateBeginEvent(self,e):

            d = (self.position.position.value).copy()

            if self.d_flag == 0 :
                d[0][self.p1] = (self.iter/self.nb_iter_d)*self.r
                d[0][self.perpen] = self.h_effector - (self.iter/self.nb_iter_d)*self.height_shift
                if self.iter >= self.nb_iter_d:
                    self.d_flag = 1
                    self.iter = 1
            else :
                # in_iter = self.iter - self.nb_iter_d
                d[0][self.p1] = self.r*cos((self.iter/self.nb_iter)*2*pi);
                d[0][self.p2] = self.r*sin((self.iter/self.nb_iter)*2*pi);


            self.position.position = [d[0]]

            self.iter += 1

class SquareTrajectory(Sofa.Core.Controller):
    """
    Génère un trajectoire carré à l'effecteur

    INPUT : 
    length = longueur d'un coté
    nb_iter = nb d'itération pour réaliser un coté du carré 
    node : noeud sur se trouve l'objet qui va effectuer un cercle
    name : nom de l'objet qui contient la position à faire varier
    square_height : hauteur du carré tracé par l'effecteur (selon l'axe z ici)
    module = variable stiff qui contient toutes les données du robot
    axis = variable qui donne le plan dans lequel on tracer le carré ([1,1,0] = x,y,  [1,0,1] = x,z)

    Exemple : rootNode.addObject(SquareTrajectory(rayon =square_radius, nb_iter = nb_iter_square,node = goal2,name = 'goal2M0',square_height = square_height+0,module=stiff))
    """
    def __init__(self,length,nb_iter,node,name,square_height,module,axis = [1,1,0],*args, **kwargs):
        Sofa.Core.Controller.__init__(self,args,kwargs)
        self.stiffNode = node # for the generic one
        self.position = self.stiffNode.getObject(name)
        self.nb_iter_d = nb_iter # nombre d'iteration pour réaliser le sement qui amène au bord u cercle
        self.nb_iter = nb_iter  # nb d'itération pour réaliser la trajectoire
        self.d_flag = 0 # flag, passe à 1 quand le placement sur le cercle est effectué
        self.iter = 1 # numérote les itérations
        self.r = length
        self.square_height = square_height
        self.h_effector = module.h_module * module.nb_module
        # self.height_shift = self.h_effector - self.circle_height
        # print(self.height_shift)

        flag_temp = 0
        for k in range(len(axis)):
            if axis[k] == 0 :
                self.perpen = k
            elif flag_temp == 0 :
                self.p1 = k
                flag_temp = 1
            else :
                self.p2 = k

    def onAnimateBeginEvent(self,e):

        d = (self.position.position.value).copy()

        # if self.d_flag == 0 :
        #     d[0][0] = (self.iter/self.nb_iter_d)*self.r
        #     d[0][2] = self.h_effector - (self.iter/self.nb_iter_d)*self.height_shift
        #     if self.iter >= self.nb_iter_d:
        #         self.d_flag = 1
        #         self.iter = 1
        # else :
        #     # in_iter = self.iter - self.nb_iter_d
        #     d[0][0] = self.r*math.cos((self.iter/self.nb_iter)*2*math.pi);
        #     d[0][1] = self.r*math.sin((self.iter/self.nb_iter)*2*math.pi);
        
        if self.d_flag == 0 :
            d[0][self.p1] = (self.iter/(self.nb_iter_d/2))*self.r
            if self.iter >= self.nb_iter_d/2:
                self.d_flag = 1
                self.iter = 0
        elif self.d_flag == 1 :
            d[0][self.p2] = (self.iter/(self.nb_iter_d/2))*self.r
            if self.iter >= self.nb_iter_d/2:
                self.d_flag = 2
                self.iter = 0
        elif self.d_flag == 2 :
            d[0][self.p1] = d[0][self.p1] - self.r/self.nb_iter_d
            if self.iter >= self.nb_iter_d*2:
                self.d_flag = 3
                self.iter = 0
        elif self.d_flag == 3 :
            d[0][self.p2] = d[0][self.p2] - self.r/self.nb_iter_d
            if self.iter >= self.nb_iter_d*2:
                self.d_flag = 4
                self.iter = 0
        elif self.d_flag == 4 :
            d[0][self.p1] = d[0][self.p1] + self.r/self.nb_iter_d
            if self.iter >= self.nb_iter_d*2:
                self.d_flag = 5
                self.iter = 0
        elif self.d_flag == 5 :
            d[0][self.p2] = d[0][self.p2] + self.r/self.nb_iter_d
            if self.iter >= self.nb_iter_d*2:
                self.d_flag = 2
                self.iter = 0

        self.position.position = [d[0]]

        self.iter += 1


class PatternTrajectory(Sofa.Core.Controller):
# Génère un trajectoire circulaire à l'effecteur # je ne sais plus si elle fonctionne => a verifier
        def __init__(self,rayon,nb_iter,child_name,name,square_height,module,*args, **kwargs):
            Sofa.Core.Controller.__init__(self,args,kwargs)
            self.RootNode = kwargs["RootNode"]
            self.stiffNode = self.RootNode.getChild(child_name) # for the generic one
            self.position = self.stiffNode.getObject(name)
            # self.stiffNode = self.RootNode.getChild('goal')
            # self.position = self.stiffNode.getObject('goalMO')
            self.nb_iter_d = 50 # nombre d'iteration pour réaliser le sement qui amène au bord u cercle
            self.nb_iter = nb_iter  # nb d'itération pour réaliser la trajectoire
            self.d_flag = 0 # flag, passe à 1 quand le placement sur le cercle est effectué
            self.iter = 1 # numérote les itérations
            self.r = rayon
            self.square_height = square_height
            self.h_effector = module.h_module * module.nb_module
            # self.height_shift = self.h_effector - self.circle_height
            # print(self.height_shift)
            # self.x = 0
            # self.y = 0
            self.ratio = 20

        def onAnimateBeginEvent(self,e):

            d = (self.position.position.value).copy()

            if self.d_flag == 0 :
                d[0][0] = -(self.iter/self.nb_iter_d)*self.r
                d[0][1] = -(self.iter/self.nb_iter_d)*self.r
                if self.iter >= self.nb_iter_d:
                    self.d_flag = 1
                    # self.iter = 1
            else :
                for x in range(2*self.r*self.ratio):
                    d[0][0] = x/self.ratio - self.r
                    for y in range(2*self.r*self.ratio):
                        if x % 2 == 0:
                            d[0][1] = y/self.ratio - self.r
                        # else :
                        #     d[0][1] = self.r - y/self.ratio



            self.position.position = [d[0]]



class LineTrajectory(Sofa.Core.Controller):
    """
    Génère un trajectoire linéaire 

    INPUT : 
    p_begin = coordonnée du point à l'extrémités du début segment
    p_end = coordonnée du point à  l'extrémités de la fin du segment
    nb_iter = nb d'itération pour réaliser un coté du carré 
    node : noeud sur se trouve l'objet qui va effectuer un cercle
    name : nom de l'objet qui contient la position à faire varier
    square_height : hauteur du carré tracé par l'effecteur (selon l'axe z ici)
    module = variable stiff qui contient toutes les données du robot

    Exemple : rootNode.addObject(LineTrajectory(nb_iter=20,node = goal2,name = 'goal2M0',p_begin = [0, 0 , 55], p_end = [10, -10 , 55]))

    """
    def __init__(self,nb_iter,node,name,p_begin = [-10 , 0 , 58], p_end = [10, 0 , 58 ] , *args, **kwargs):
        Sofa.Core.Controller.__init__(self,args,kwargs)

        self.stiffNode = node # for the generic one
        self.position = self.stiffNode.getObject(name)

        self.nb_iter = nb_iter  # nb d'itération pour réaliser la trajectoire
        self.iter = 0 # numérote les itérations

        self.begin = p_begin 
        self.end = p_end

    def onAnimateBeginEvent(self,e):

        if self.iter <= self.nb_iter :

            d = (self.position.position.value).copy()

            d[0][0] = ((self.nb_iter - self.iter)/self.nb_iter)*self.begin[0] + (self.iter/self.nb_iter)*self.end[0]
            d[0][1] = ((self.nb_iter - self.iter)/self.nb_iter)*self.begin[1] + (self.iter/self.nb_iter)*self.end[1]
            d[0][2] = ((self.nb_iter - self.iter)/self.nb_iter)*self.begin[2] + (self.iter/self.nb_iter)*self.end[2]

            self.position.position = [d[0]]

        self.iter += 1


class PointPerPointTrajectory(Sofa.Core.Controller):
    """
    Génère un trajectoire, contenant tous les points passés en argument

    INPUT : 
    point_tab = tableau des points à atteindre successivements
    err_d = erreur désirée => si elle est très grande, va juste rester le nb self.err_nb d'iteration à chaque position avant de passer à la suivante.
    nb_iter = nb d'itération pour réaliser un coté du carré 

    node : noeud sur se trouve l'objet qui va effectuer un cercle
    name : nom de l'objet qui contient la position à faire varier

    node_pos : noeud sur se trouve l'objet qui va mesure la position atteinte (en réalité et en simulation) => utilisés pour comprarer les positions désirés et les positions 
    name_pos : nom de l'objet qui contient la position 
    type_flag : pour savoir si le noeud passé en argument node_pos et name_pos est ou non une poutre (type_flag = 0 -> goal_point; type_flag = 1 -> poutre, type_flag = 2 -> fem avec indices des points a moyenner)
    indices : utilisés seulement dans le cas du FEM, pour savoir quels indices utiliser

    module = variable stiff qui contient toutes les données du robot
    shift  = décalage !!! A METTRE AU CLAIR !!! surement pour le décalage entre goal et goal2 -> vraiment utile ?

    Exemple : rootNode.addObject(PointPerPointTrajectory(node = goal,name = 'goalM0',module = stiff,point_tab = point_tab, node_pos = rigidFramesNode, name_pos = 'DOFs',err_d = 50,shift=0,beam_flag = 1))

    """
    def __init__(self, point_tab, err_d, node, name, node_pos, name_pos,module, shift,type_flag=0,indices = "null",loop_flag = False,*args, **kwargs):
        Sofa.Core.Controller.__init__(self,args,kwargs)

        self.stiffNode = node # for the generic one
        self.position = self.stiffNode.getObject(name)

        self.stiffNode_pos = node_pos # for the generic one
        self.position_pos = self.stiffNode_pos.getObject(name_pos)

        self.iter = 1 # numérote les itérations

        self.p_tab = point_tab 

        self.flag = 0 # to know which point in the tab will we reach

        self.nb_poutre = module.nb_poutre

        self.err_d = err_d # desired error
        self.err_nb = 1 # it will recquire 50 iteration with the good error to go to the next point # mettre en argument de la fonction
        self.err_incr = 0

        self.shift = shift

        self.type_flag = type_flag
        self.loop_flag = loop_flag

        if type_flag == 2:
            self.indices = indices

    def onAnimateBeginEvent(self,e):

        if self.type_flag == 1 :
            pos = self.position_pos.position.value[self.nb_poutre-1][0:3]
        elif self.type_flag == 0 :
            pos = self.position_pos.position[0]
        elif self.type_flag == 2 :
            [posx,posy,posz] = connect.get_mean_point(all_positions = self.position_pos,indices = self.indices)
            pos = [posx,posy,posz]

        d = (self.position.position.value).copy()


        d[0][0] = self.p_tab[self.flag][0]
        d[0][1] = self.p_tab[self.flag][1]
        d[0][2] = self.p_tab[self.flag][2]

        # print('d is')
        # print(d)

        self.position.position = [d[0]]

        d2 = d
        d2[0][2] = self.p_tab[self.flag][2] - self.shift

        #erreur = np.linalg.norm(pos-d2[0])
        erreur = 0
        # print(erreur)

        #if self.iter % 10 == 0:
            #self.flag += 1
                #if self.flag >= len(self.p_tab):
                     #self.flag =0

        if erreur < self.err_d :
            self.err_incr += 1
            if self.err_incr >= self.err_nb :
                self.flag += 1
                self.err_incr = 0
                if self.loop_flag == True :
                    if self.flag >= len(self.p_tab):
                        self.flag =0
                    
        #else :
            #self.err_incr = 0

        self.iter += 1

class PointPerPointTrajectory_Synchro(Sofa.Core.Controller):
    """
    Génère un trajectoire, contenant tous les points passés en argument

    INPUT : 
    point_tab = tableau des points à atteindre successivements
    err_d = erreur désirée => si elle est très grande, va juste rester le nb self.err_nb d'iteration à chaque position avant de passer à la suivante.
    nb_iter = nb d'itération pour réaliser un coté du carré 

    node : noeud sur se trouve l'objet qui va effectuer un cercle
    name : nom de l'objet qui contient la position à faire varier

    node_pos : noeud sur se trouve l'objet qui va mesure la position atteinte (en réalité et en simulation) => utilisés pour comprarer les positions désirés et les positions 
    name_pos : nom de l'objet qui contient la position 
    type_flag : pour savoir si le noeud passé en argument node_pos et name_pos est ou non une poutre (type_flag = 0 -> goal_point; type_flag = 1 -> poutre, type_flag = 2 -> fem avec indices des points a moyenner)
    indices : utilisés seulement dans le cas du FEM, pour savoir quels indices utiliser

    module = variable stiff qui contient toutes les données du robot
    shift  = décalage !!! A METTRE AU CLAIR !!! surement pour le décalage entre goal et goal2 -> vraiment utile ?

    Exemple : rootNode.addObject(PointPerPointTrajectory(node = goal,name = 'goalM0',module = stiff,point_tab = point_tab, node_pos = rigidFramesNode, name_pos = 'DOFs',err_d = 50,shift=0,beam_flag = 1))

    """
    def __init__(self, point_tab, err_d, node, name, node_pos, name_pos,module, shift,synchro_flag,type_flag=0,indices = "null",loop_flag = False,print_flag = False,check_flag = False,*args, **kwargs):
        Sofa.Core.Controller.__init__(self,args,kwargs)

        self.stiffNode = node # for the generic one
        self.position = self.stiffNode.getObject(name)

        self.stiffNode_pos = node_pos # for the generic one
        self.position_pos = self.stiffNode_pos.getObject(name_pos)

        self.iter = 1 # numérote les itérations

        self.p_tab = point_tab 

        self.pos_ind = 0 # to know which point in the tab will we reach (indices)

        self.nb_poutre = module.nb_poutre

        self.err_d = err_d # desired error
        self.err_nb = 5 # it will recquire 50 iteration with the good error to go to the next point # mettre en argument de la fonction
        self.err_incr = 0

        self.shift = shift

        self.type_flag = type_flag
        self.loop_flag = loop_flag
        self.synchro_flag = synchro_flag
        print(self.synchro_flag)
        self.print_flag = print_flag

        if type_flag == 2:
            self.indices = indices

        self.check_flag = check_flag

    def onAnimateBeginEvent(self,e):

        if self.type_flag == 1 :
            pos = self.position_pos.position.value[self.nb_poutre-1][0:3]
        elif self.type_flag == 0 :
            pos = self.position_pos.position[0]
        elif self.type_flag == 2 :
            [posx,posy,posz] = connect.get_mean_point(all_positions = self.position_pos,indices = self.indices)
            pos = [posx,posy,posz]

        d = (self.position.position.value).copy()


        d[0][0] = self.p_tab[self.pos_ind][0]
        d[0][1] = self.p_tab[self.pos_ind][1]
        d[0][2] = self.p_tab[self.pos_ind][2]

        # print('d is')
        # print(d)

        self.position.position = [d[0]]

        d2 = d
        d2[0][2] = self.p_tab[self.pos_ind][2] - self.shift # on réapplique le décalage

        if self.check_flag == True :
             if (utils.check_position_error(p1 = d2[0], p2 = pos, threshold = self.err_d)) :
                self.pos_ind += 1
        else :
            erreur = np.linalg.norm(pos-d2[0])

            if erreur < self.err_d : # si l'erreur est plus petite que l'erreur désirée
                self.err_incr += 1 # nom de variable peu clair, qui compte le nombre de fois ou on est en dessous de l'erreur désirée
                if self.err_incr >= self.err_nb : # si elle est plus petite sur un nombre de pas de temps suffisant, on passe au point suivant
                    if self.print_flag == True :
                        print('PointperPoint_synchro : position ok, move to the next point')
                    self.pos_ind += 1
                    self.err_incr = 0
                    self.synchro_flag.synchro_flag = True
                    self.synchro_flag.synchro_flag2 = True
                    # print(self.synchro_flag)
                    if self.loop_flag == True :
                        if self.pos_ind >= len(self.p_tab):
                            if print_flag == True :
                                print('PointperPoint_synchro : tab finished, restart in a loop')
                            self.pos_ind =0
            else : # sinon on reset le compteur => on doit être dans la position sur un nb de trames successive (si on vuet retirer la successivité du problème, ajouter une condition if successive_flag == 1 avant de reinitialiser le compteur)
                self.err_incr = 0
                    


        self.iter += 1