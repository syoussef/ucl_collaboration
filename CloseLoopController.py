# from operator import ge
import Sofa
# from splib3.numerics import Quat
from numpy import *
# from time import clock

import QuaternionUtils as QU
from splib3.numerics import quat as qt

import utils as utils # for Embedded Control (Tanguy)

# import statusvars

class CloseLoopController(Sofa.Core.Controller):


    def __init__(self, K_P,K_I,print_flag=False,*args, **kwargs):
        Sofa.Core.Controller.__init__(self, *args, **kwargs)
         
        self.RootNode = kwargs['RootNode']

        # consigne a manipuler
        self.inputMO1 = self.RootNode.DesiredPosition.DesiredPositionM0
        # self.outputMO1 = self.RootNode.goal.goalMO
        self.trackingMO1 = self.RootNode.MeasuredPosition.MeasuredPositionM0

        self.stiffNode = self.RootNode.getChild('goal') # for the generic one
        self.outputMO1 = self.stiffNode.getObject('goalM0')

        self.K_P = K_P # P proportionnal coefficient to avoid over correction
        self.K_I = K_I

        self.first_flag = 0 # just to avoid the 1st time step
        self.outputPos = array(self.outputMO1.position[0])

        # print("AAAAAAAA")
        # print(self.outputPos)

        self.maximum_error_integration = 200

        self.integrate_error = 0

    def onAnimateBeginEvent(self,dt):

        ########################
        # TIP SENSOR (rotation)
        ########################

        if self.first_flag == 0 :
            self.first_flag = 1
        else :

            inputPos = array(self.inputMO1.position[0])
            trackingPos = array(self.trackingMO1.position[0])
            # outputPos = array(self.outputMO1.position[0])

            error = inputPos - trackingPos

            self.integrate_error = self.integrate_error + error
            for index in range (3): 
                if self.integrate_error[index] > maximum_error_integration:
                     self.integrate_error[index] = maximum_error_integration
                if self.integrate_error[index] < -maximum_error_integration:
                     self.integrate_error[index] = -maximum_error_integration  

            self.outputPos[0] = self.outputPos[0] + 2*error[0]*self.K_P + 1.5*self.integrate_error[0]*self.K_I
            self.outputPos[1] = self.outputPos[1] + 2*error[1]*self.K_P + 1.5*self.integrate_error[1]*self.K_I
            self.outputPos[2] = self.outputPos[2] + error[2]*self.K_P + self.integrate_error[2]*self.K_I

            if self.print_flag == True :
                # print("- Debut trame -")
                # print("Tracking posistion :")
                # print(trackingPos)
                # print("Input posistion :")
                # print(inputPos)
                # print("Error :")
                # print(error)        
                # print('the integrate_error is')
                # print(self.integrate_error)
                # print('Output position')
                # print(self.outputPos)
                # print("- Fin trame -")


                print("Tracking Pos\t Input Pos \t Error \t outputPos")
                print("{0}\t{1}\t{2}".format(trackingPos, inputPos,error,self.outputPos ))

            # outputPos = [outputPos[0],outputPos[1],outputPos[2]]

            # print([outputPos])

            self.outputMO1.position = [self.outputPos]


class CloseLoopController2(Sofa.Core.Controller):


    def __init__(self, K_P,K_I,print_flag = False,*args, **kwargs):
        Sofa.Core.Controller.__init__(self, *args, **kwargs)
         
        self.RootNode = kwargs['RootNode']

        # consigne a manipuler
        self.inputMO1 = self.RootNode.DesiredPosition.DesiredPositionM0
        # self.outputMO1 = self.RootNode.goal.goalMO
        self.trackingMO1 = self.RootNode.MeasuredPosition.MeasuredPositionM0

        self.stiffNode = self.RootNode.getChild('goal') # for the generic one
        self.outputMO1 = self.stiffNode.getObject('goalM0')

        self.K_P = K_P # P proportionnal coefficient to avoid over correction
        self.K_I = K_I

        self.first_flag = 0 # just to avoid the 1st time step
        self.outputPos = array(self.outputMO1.position[0])

        # print("controller 2")
        # print(self.outputPos)

        self.integrate_error = 0

        self.maximum_error_integration = 500

        self.print_flag = print_flag

    def onAnimateBeginEvent(self,dt):

        ########################
        # TIP SENSOR (rotation)
        ########################

        if self.first_flag == 0 :
            self.first_flag = 1
        else :

            inputPos = array(self.inputMO1.position[0])
            trackingPos = array(self.trackingMO1.position[0])
            # outputPos = array(self.outputMO1.position[0])

            # if (math.isnan(trackingPos[0]) == 0): # Pourquoi math.isnan ?
            #     print("Tracking posistion is")
            # # print(outputPos)
            #     print(trackingPos)
            # #print(inputPos)

            error = -(trackingPos[0:3] - inputPos)

            self.integrate_error = self.integrate_error + error
            for index in range (3): 
                if self.integrate_error[index] > self.maximum_error_integration :
                    self.integrate_error[index] = self.maximum_error_integration 
                if self.integrate_error[index] < - (self.maximum_error_integration) :
                    self.integrate_error[index] = - (self.maximum_error_integration)           

            self.outputPos[0] = inputPos[0] + 2*error[0]*self.K_P + 1.5*self.integrate_error[0]*self.K_I
            self.outputPos[1] = inputPos[1] + 2*error[1]*self.K_P + 1.5*self.integrate_error[1]*self.K_I
            self.outputPos[2] = inputPos[2] + 1.2*error[2]*self.K_P + 1.2*self.integrate_error[2]*self.K_I

            if self.print_flag == True :
                print("- Debut trame -")
                print("Tracking posistion :")
                print(trackingPos)
                print("Input posistion :")
                print(inputPos)
                print("Error :")
                print(error)        
                print('the integrate_error is')
                print(self.integrate_error)
                print('Output position')
                print(self.outputPos)
                print("- Fin trame -")
                # print("Tracking Pos\t Input Pos \t Error \t outputPos")
                # print("{0:f}\t{1:f}\t{2:f}\t{4:f}".format(*trackingPos, *inputPos,*error,*self.outputPos ))
            
            self.outputMO1.position = [self.outputPos]



class CloseLoopController_Orientation(Sofa.Core.Controller):

    def __init__(self,node_tracking,name_tracking,node_desired,name_desired,node_command,name_command, K_P,K_I,print_flag = False,*args, **kwargs):
        Sofa.Core.Controller.__init__(self, *args, **kwargs)
         
        self.node_tracking = node_tracking 
        self.tracking_object = self.node_tracking.getObject(name_tracking)

        self.node_desired = node_desired 
        self.desired_object = self.node_desired.getObject(name_desired)

        self.node_command = node_command 
        self.command_object = self.node_command.getObject(name_command)

        self.K_P = K_P # P proportionnal coefficient to avoid over correction
        self.K_I = K_I

        self.first_flag = 0 # just to avoid the 1st time step # why ?
        self.command_pos = array(self.command_object.position[0])

        print("controller orientation")
        # print(self.command_pos)

        self.maximum_error_integration = 500

        self.integrate_error = 0

        self.print_flag = print_flag

    def onAnimateBeginEvent(self,dt):

        ########################
        # TIP SENSOR (rotation)
        ########################

        if self.first_flag == 0 :
            self.first_flag = 1
        else :

            desired_pos = array(self.desired_object.position[0]) # on récupère la position
            desired_quat = qt.Quat(QU.getQuatFromPos(desired_pos)) # extrait le quaternion qui représente l'orientation
            desired_quat = QU.alignQuatX(desired_quat) # supprime la rotation selon l'axe x
         #   print(desired_quat[0])
            QU.applyQuatToPos(desired_quat,desired_pos) # on remet le quaternion dans la variable pos, qui contient [x, y, z + quat]
          #  print(desired_pos)

            tracking_pos = array(self.tracking_object.position[0])
            tracking_quat = qt.Quat(QU.getQuatFromPos(tracking_pos))
            tracking_quat = QU.alignQuatX(tracking_quat)
            QU.applyQuatToPos(tracking_quat,tracking_pos)

            # if (math.isnan(tracking_pos[0]) == 0): # why ?
            #     print("Tracking posistion is")
            # # print(outputPos)
            #     print(trackingPos)
            # #print(inputPos)

            error = -(tracking_pos - desired_pos)

            self.integrate_error = self.integrate_error + error
            for index in range (3): 
                if self.integrate_error[index] > self.maximum_error_integration :
                    self.integrate_error[index] = self.maximum_error_integration 
                if self.integrate_error[index] < -self.maximum_error_integration :
                    self.integrate_error[index] = -self.maximum_error_integration           

            self.command_pos[0] = desired_pos[0] + error[0]*self.K_P + self.integrate_error[0]*self.K_I # position
            self.command_pos[1] = desired_pos[1] + error[1]*self.K_P + self.integrate_error[1]*self.K_I
            self.command_pos[2] = desired_pos[2] + error[2]*self.K_P + self.integrate_error[2]*self.K_I

            self.command_pos[3] = desired_pos[3] + error[3]*self.K_P*0.01 + self.integrate_error[3]*self.K_I*0.5  # orientation
            self.command_pos[4] = desired_pos[4] + error[4]*self.K_P*0.01 + self.integrate_error[4]*self.K_I*0.5 
            self.command_pos[5] = desired_pos[5] + error[5]*self.K_P*0.01 + self.integrate_error[5]*self.K_I*0.5 
            self.command_pos[6] = desired_pos[5] + error[6]*self.K_P*0.01 + self.integrate_error[6]*self.K_I*0.5 

            if self.print_flag == True : # mettre printer dans une fonction séparé ? (pour factoriser entre les controller ?)
                # print(outputPos)
                print("- Debut trame -")
                print("Tracking posistion :")
                print(trackingPos)
                print("Input posistion :")
                print(inputPos)
                print("Error :")
                print(error)        
                print('the integrate_error is')
                print(self.integrate_error)
                print('Output position')
                print(self.outputPos)
                print("- Fin trame -")

            self.command_object.position = [self.command_pos]

class CloseLoopController_Embedded(Sofa.Core.Controller):

    def __init__(self,node_tracking,name_tracking,node_desired,name_desired,node_command,name_command, K_P,K_I,print_flag = False,*args, **kwargs):
        Sofa.Core.Controller.__init__(self, *args, **kwargs)
         
        self.node_tracking = node_tracking 
        self.tracking_object = self.node_tracking.getObject(name_tracking)

        self.node_desired = node_desired 
        self.desired_object = self.node_desired.getObject(name_desired)

        self.node_command = node_command 
        self.command_object = self.node_command.getObject(name_command)

        self.PID = utils.PID( P = K_P, I = K_I, D = 0)

        self.first_flag = 0 # just to avoid the 1st time step # why ?
        self.command_pos = array(self.command_object.position[0])

        print("controller Tanguy")
        # print(self.command_pos)

        self.maximum_error_integration = 500

        self.integrate_error = 0

        self.print_flag = print_flag

    def onAnimateBeginEvent(self,dt):

        ########################
        # TIP SENSOR (rotation)
        ########################

        if self.first_flag == 0 :
            self.first_flag = 1
        else :

            desired_pos = array(self.desired_object.position[0]) # on récupère la position

            tracking_pos = array(self.tracking_object.position[0])

            self.command_pos = self.PID.compute_correction

            if self.print_flag == True : # mettre printer dans une fonction séparé ? (pour factoriser entre les controller ?)
                # print(outputPos)
                print("- Debut trame -")
                print("Tracking posistion :")
                print(trackingPos)
                print("Input posistion :")
                print(inputPos)
                # print("Error :")
                # print(error)        
                print('the integrate_error is')
                print(self.integrate_error)
                print('Output position')
                print(self.command_pos)
                print("- Fin trame -")

            self.command_object.position = [self.command_pos]


## CODE INITIAL (ALESSANDRINI) ##

# class CloseLoopController(Sofa.Core.Controller):


#     def __init__(self, *args, **kwargs):
#         Sofa.Core.Controller.__init__(self, *args, **kwargs)
         
#         self.RootNode = kwargs['RootNode']

#         # consigne a manipuler
#         self.inputMO1 = self.RootNode.Consigne1.Consigne1
#         self.outputMO1 = self.RootNode.QPTarget1.QPTarget1
#         self.trackingMO1 = self.RootNode.Tracking1.Tracking1
#         self.inputMO2 = self.RootNode.Consigne2.Consigne2
#         self.outputMO2 = self.RootNode.QPTarget2.QPTarget2
#         self.trackingMO2 = self.RootNode.Tracking2.Tracking2



#         self.kp_r1 = 2
#         self.ki_r1 = 2

#         self.kp_r2 = 1
#         self.ki_r2 = 1

#         self.kp_t2x = 0.00
#         self.ki_t2x = 0.8
#         self.kp_t2yz = 0.0
#         self.ki_t2yz = 0.5


#         # verrouillage des rotations (TEMP)
#         # self.kp_r1 = 0
#         # self.ki_r1 = 0
#         # self.kp_r2 = 0
#         # self.ki_r2 = 0


#         self.lastClock = clock()

#         self.q_ki_r1 = Quat([0,0,0,1])	# rotation integree
#         self.q_ki_r2 = Quat([0,0,0,1])	# rotation integree
#         self.t_ki_t2 = [0,0,0] # translation integree

#         return 0

#     def applyDeadZone(self,x,width):
#         if (x>0):
#             if (x<width):
#                 return 0
#             else:
#                 return x-width
#         else:
#             if (x>-width):
#                 return 0
#             else:
#                 return x+width

                


#     def onAnimateBeginEvent(self,dt):

#         if statusvars.close_loop==False:
#             # TODO:
#             if statusvars.actuatorsON==True:
#                 # reset integrators ONLY IF ACTUATORS STILL ON
#                 self.q_ki_r1 = Quat([0,0,0,1])	# rotation integree
#                 self.q_ki_r2 = Quat([0,0,0,1])	# rotation integree
#                 self.t_ki_t2 = [0,0,0] # translation integree
#             return None	        
        

#         c = clock()
#         real_dt = c - self.lastClock
#         self.lastClock = c
#         if (real_dt > 0.05):
#             real_dt = 0.05

#         # print('dt'+str(dt))


#         ########################
#         # TIP SENSOR (rotation)
#         ########################

#         inputPos = array(self.inputMO1.position[0])
#         trackingPos = array(self.trackingMO1.position[0])

#         q_tracking = getQuatFromPos(trackingPos)
#         q_input = getQuatFromPos(inputPos)        
#         q_input = alignQuatX(q_input)	# annulation de la rotation en X
#         applyQuatToPos(q_input,inputPos) # update input position to apply roll reset
#         self.inputMO1.position = [inputPos]

#         # KP
        
#         # ecart position reelle-consigne
#         q_kp = Quat.product( q_input, q_tracking.getConjugate() )
#         axis_angle = q_kp.getAxisAngle()
#         axis_angle[1]*=self.kp_r1
#         q_kp = Quat.createFromAxisAngle(axis_angle[0],axis_angle[1])


#         # KI
        
#         # ecart position reelle-consigne
#         q_slerp = Quat.slerp(q_tracking,q_input,self.ki_r1*real_dt)
#         q = Quat.product( q_slerp, q_tracking.getConjugate() )
        
#         self.q_ki_r1 = Quat.product(self.q_ki_r1,q)

#         # cap q_ki
#         axis_angle = self.q_ki_r1.getAxisAngle()
#         if (axis_angle[1]>1.5):
#             axis_angle[1]=1.5
#             self.q_ki_r1 = Quat.createFromAxisAngle(axis_angle[0],axis_angle[1])

#         self.q_ki_r1 = alignQuatX(self.q_ki_r1)

#         # on applique
#         q_commande = Quat.product(q_kp,self.q_ki_r1)
#         outputPos=[0]*7
#         outputPos[0]=inputPos[0]
#         outputPos[1]=inputPos[1]
#         outputPos[2]=inputPos[2]
#         applyQuatToPos(q_commande,outputPos)
#         self.outputMO1.position = [outputPos]

#         ########################
#         # MIDDLE SENSOR (rotation)
#         ########################

#         inputPos = array(self.inputMO2.position[0])
#         trackingPos = array(self.trackingMO2.position[0])

#         q_tracking = getQuatFromPos(trackingPos)
#         q_input = getQuatFromPos(inputPos)        
#         q_input = alignQuatX(q_input)	# annulation de la rotation en X
#         applyQuatToPos(q_input,inputPos) # update input position to apply roll reset
#         self.inputMO2.position = [inputPos]

#         # KP
        
#         # ecart position reelle-consigne
#         q_kp = Quat.product( q_input, q_tracking.getConjugate() )
#         axis_angle = q_kp.getAxisAngle()
#         axis_angle[1]*=self.kp_r2
#         q_kp = Quat.createFromAxisAngle(axis_angle[0],axis_angle[1])


#         # KI
        
#         # ecart position reelle-consigne
#         q_slerp = Quat.slerp(q_tracking,q_input,self.ki_r2*real_dt)
#         q = Quat.product( q_slerp, q_tracking.getConjugate())
        
#         self.q_ki_r2 = Quat.product(self.q_ki_r2,q)

#         # cap q_ki
#         axis_angle = self.q_ki_r2.getAxisAngle()
#         if (axis_angle[1]>1.5):
#             axis_angle[1]=1.5
#             self.q_ki_r2 = Quat.createFromAxisAngle(axis_angle[0],axis_angle[1])

#         self.q_ki_r2 = alignQuatX(self.q_ki_r2)

#         ########################
#         # MIDDLE SENSOR (translation)
#         ########################

#         inputPos = array(self.inputMO2.position[0])
#         trackingPos = array(self.trackingMO2.position[0])
        
#         # ecart position reelle-consigne
#         epsilon = [inputPos[0]-trackingPos[0],inputPos[1]-trackingPos[1],inputPos[2]-trackingPos[2]]

#         # dead zone sur epsilon !
#         epsilon[0] = self.applyDeadZone(epsilon[0],5)

#         # KP
#         t_kp = [epsilon[0]*self.kp_t2x,epsilon[1]*self.kp_t2yz,epsilon[2]*self.kp_t2yz]

#         # KI
#         self.t_ki_t2[0]+=epsilon[0]*self.ki_t2x*real_dt
#         self.t_ki_t2[1]+=epsilon[1]*self.ki_t2yz*real_dt
#         self.t_ki_t2[2]+=epsilon[2]*self.ki_t2yz*real_dt

#         # cap q_ki
#         # axis_angle = Quat.quatToAxis(self.q_ki_r2)
#         # if (axis_angle[1]>1.5):
#         # 	axis_angle[1]=1.5
#         # 	self.q_ki_r2 = Quat.axisToQuat(axis_angle[0],axis_angle[1])

#         # self.q_ki_r2 = alignQuatX(self.q_ki_r2)

#         # on applique
#         q_commande = Quat.product(q_kp,self.q_ki_r2)
#         outputPos=[0]*7
#         outputPos[0] = inputPos[0]+t_kp[0]+self.t_ki_t2[0]
#         outputPos[1] = inputPos[1]+t_kp[1]+self.t_ki_t2[1]
#         outputPos[2] = inputPos[2]+t_kp[2]+self.t_ki_t2[2]
#         applyQuatToPos(q_commande,outputPos)
#         self.outputMO2.position = [outputPos]

#         return 0
